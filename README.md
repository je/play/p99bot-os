<h1><div align="center">Project 1999 Bot - Operating System Container</div></h1>

[TOC]

# Overview

This container provides separation of concerns and a basis for
customization by other containers based on this one. This
container is currently based on the latest available version of
Ubuntu Linux.

# Creation

## Building the Container

For basic information about how to build this container, see the
main project repo[^1].

This container is built from an exceedingly simple Dockerfile.
The Dockerfile installs Ubuntu, copies the `init` script and
configures the default startup command (the `init` script).

# Operation

## Preparation

For basic information about how to prepare a host to run this
container, see the main project repo[^1].

## Configuration

The container is configured by setting environment variables when
running the container (e.g. via the `-e` switch when using the
`docker run` command). Available environment variables are:

* **P99BOT_NOSLEEP**: During normal non-interactive startup, the
container will sleep indefinitely in order to keep the container
running. One can set the environment variable P99BOT_NOSLEEP to
any value to prevent this sleep behavior. This variable is unset
by default.

## Usage

For basic information about how to use this container, see the
main project repo[^1].

---

[^1]: https://gitlab.com/je/play/p99bot
